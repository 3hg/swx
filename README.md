swx - générateur de site statique
=================================


swx permet de créer un site statique à partir d'une arborescence de fichiers
écrits au format [txt2tags](http://txt2tags.org) ou 
[markdown](http://daringfireball.net/projects/markdown/syntax). Notez toutefois
que n'importe quel autre convertisseur et langage markup,
(pandoc…) peut être utilisé.

Mis à part ce convertisseur, aucun outil spécial n'est nécessaire, on n'utilise
que les commandes de base du système (sed, cut, echo...)

Il s'agit en fait d'un kit de scripts qui ont pour chacun une tâche précise.
“Diviser pour mieux régner” qu'ils disaient… Au moins, chaque script fait son
boulot, et le fait bien.

Inspiré par [sw](https://github.com/jroimartin/sw), swx générera vos pages html,
créera des liens entre elles sous forme de menu, et au lieu d'utiliser des
modèles avec jinja, vous pouvez utiliser des simples feuilles de style css et
préciser au script des morceaux de html que vous voudriez voir dans chaque page.


Dépendances
-----------

Vous aurez besoin d'un shell type bash, zsh, ksh et d'un convertisseur
(markdown, discount...).

Fonctionnalités
------------------


- Clone de la structure d'un site
- Conversion des fichiers markdown, txt2tags en html
- Ajout d'un menu de navigation entre les pages
- Ajout des nouvelles pages à la première page du site
- Peut ignorer des fichiers d'une liste définie dans le fichier de configuration
- Création d'un flux atom : [voir swx_atom](#swx_atom)
- Création d'un page de blog : [voir swx_blog](#swx_blog)
- Génération d'un “plan de site” listant tout ce qui y est disponible. [voir swx_plan](#swx_plan)
- Génération d'un “plan de site” lisible par les robots (pour l'indexation des moteurs de recherche) [voir swx_sitemap](#swx_sitemap)
- Création de pages selon un modèle (voir [template](#template))
- Utiliser un Makefile pour simplifier la génération du site [voir Makefile](#Makefile)

Exemple : Si votre répertoire source est ainsi :

	/home/user/site/
		|style.css
		|--------------directory/
		|-----------------------index.md
		|-----------------------file1.md
		|-----------------------image.jpg
		|--------------directory2/
		|-----------------------index.md
		|-----------------------article.md


Il deviendra en sortie

	/home/user/site.static/
		|style.css
		|--------------directory/
		|-----------------------index.html
		|-----------------------file1.html
		|-----------------------image.jpg
		|--------------directory2/
		|-----------------------index.html
		|-----------------------article.html


Installation
----------------



Téléchargez tous les fichiers présents sur 
[le dépôt](https://framagit.org/3hg/swx). Une 
[archive](https://framagit.org/3hg/swx/-/archive/master/swx-master.tar.gz) est disponible.

Installez de quoi traiter votre langage markup (markdown, discount, txt2tags…)


Utilisation
------------

Le script se lance simplement ainsi, dans le répertoire qui contient le dossier contenant votre site :

	./swx repertoire_contenant_le_site

Ensuite, libre à vous de copier votre site ou le mettre à jour de la façon que vous préférez

- Gestionnaire de versions (mercurial, git)
- rsync (avec l'option -c pour n'uploader que les modifications)
- ftp

Configuration
-------------

La configuration se déroule dans le fichier swx.conf

Vous pouvez modifier le contenu des variables (en majuscule) pour définir:

 - Le titre de votre site (``TITLE``) et le sous-titre (``SUBTITLE``)
 - L'adresse du site dans ``HOSTNAME``
 - Des fichiers à ne pas lister dans le menu (``BL``)
 - ``CONVERTER`` : Le programme qui servira à convertir vos fichiers source . Pour txt2tags : ``CONVERTER='txt2tags -t html -o- --no-headers'``.
 - L'extension des fichier à convertir (défaut : `EXT=".md"`).
- `TOREPLACE='^\${_.*_}$'` : L'expression régulière des morceaux à
  remplaces dans le fichier template. Laissez ainsi en cas de doute.
Vous pouvez définir l'apparence souhaitée en modifiant le fichier style.css. Un exemple est fourni avec swx.
- `MAX=10` : Le nombre d'éléments à mettre dans le flux atom et la page
  de blog.
- `HOSTNAME=` : Le nom de domaine de votre site
- `PROTOCOL="https://"` : Pas de surprise, si?

Et les paramètres facultatifs suivants sont disponibles : 

- `NEWSSTR="%%%BLOOOOG%%%"` : La chaîne de caractère qui sera remplacée
  par une liste des nouveaux articles. Pensez à la mettre dans votre
  page de blog.
- `NEWSPAGE="/index.md"` : La page utilisée comme blog.


Le modèle <span id="template"></span>
-------------------------------

PLacez un fichier `swx.template` qui contiendra le modèle des pages de
votre site. De cette façon, vous pouvez préciser les feuilles de style
utilisées (CSS) et l'emplacement des divers éléments.

Exemple de template : 


	<!doctype html>
	<html lang="fr">
	<head>
	${_PAGETITLE_}
	<link rel="icon" href="/favicon.png" type="image/png">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="${_DESCRIPTION_}">
	<link rel="stylesheet" type="text/css" href="/style.min.css">
	<link href="/feed.atom" type="application/atom+xml" rel="alternate" title="Atom feed " />
	</head>

	<body>
	<header>
	${_HEADER_}
	<nav id="menubar">
	<details open id="menubarcontent">
	<summary id="navbar">
	<a href='/' class="root">/</a>
	${_NAVPATH_}
	</summary>
	${_MENU_}
	</details>
	</nav>
	</header>
	<main>
	<div id="article">
	${_MAIN_}
	</div>
	</main>
	<footer>
	Le 
	${_GENDATE_}
	avec <a href="https://3hg.fr/Scripts/swx">swx</a>
	</footer>
	<script src="/js/js.min.js"></script>
	</body>
	</html>



- `_PAGETITLE_` : Le titre de la page
- `_HEADER_` : Les entêtes avec les titres.
- `_DESCRIPTION_` : La description de la page (4 premières lignes)
- `_MENU_` : Un menu listant les documents dans le même répertoire que
  la page
- `_MAIN_` : La page convertie 
- `_GENDATE_` : La date de génération de la page

Exemple d'utilisation
--------------------

C'est parti, voyons en quelques étapes comment utiliser toutes les fonctionnalités de swx.

Tout d'abord, on va créer un dossier pour pouvoir travailler :

	mkdir dossier
	cd dossier


On télécharge swx, on le décompresse :

	wget -O swx.tgz https://framagit.org/3hg/swx/repository/master/archive.tar.gz
	tar xvzf swx.tgz

On crée un nouveau dossier qui contiendra notre site, puis on y crée quelques pages

	mkdir monsite
	vim monsite/index.md
	vim monsite/page1.md
	mkdir monsite/dossier1
	vim monsite/dossier1/index.md


On génère une première fois notre site :

	./swx monsite 


Apparaît alors un dossier ``monsite.static`` qui contient toutes les pages
converties en html, et un nouveau fichier ``swx.log`` qui contient la liste des
nouvelles pages (ça nous servira plus tard).


Ajout d'un flux atom <span id="swx_atom"></span>
-----------------------------------------

Pour ajouter un flux atom, on va créer un fichier atom.xml à la racine du site.

On utilise le script ``swx_atom`` fournit avec swx ainsi :

	./swx_atom SOURCEDIR > DESTDIR/feed.atom

Remplacez ``DESTDIR`` par le dossier qui contiendra le site. Dans notre exemple,
c'est ``monsite.static``



Ajout d'un page type blog <span id="swx_blog"></span>
-------------------------------------------

Vous pouvez créer une page de type blog, contenant les articles récents. Il faut
lancer : 

	./swx_blog $(SOURCEDIR) 

Si le fichier ``NEWSPAGE`` définit dans le fichier de configuration contient "%%%BLOG%%%" (ou ce que vous aurez définit
dans la configuration), alors cette ligne sera remplacée par la liste des
articles récents.


Ajout d'une page de plan de site <span id="swx_plan"></span>
------------------------------------------------------

Pour générer un plan de votre site que les visiteurs pourront utiliser afin de le parcourir, vous pouvez utiliser le script ``swx_plan`` fournit avec swx.

Pour l'utiliser :

    ./swx_plan DESTDIR > DESTDIR/Divers/Plan_du_site.html

Remplacez ``DESTDIR`` par le dossier qui contiendra le site. Dans notre exemple, c'est ``monsite.static``

N'hésitez pas à éditer le script pour modifier l'apparence de la page générée. Il utilise par défaut le style.css de votre site.


Ajout d'une page de plan de site lisible par les robots <span id="swx_sitemap"></span>
--------------------------------------------------------------------------------

Tout simplement, pour créer un sitemap lisible par les moteurs de recherche :

    ./swx_sitemap DESTDIR > DESTDIR/sitemap.xml


Que vous pouvez compresser ensuite ainsi :

    gzip --best -c DESTDIR/sitemap.xml > DESTDIR/sitemap.gz

N'oubliez pas de rajouter dans le fichier robots.txt ceci :

	User-agent: *
	Disallow:
	Sitemap: http://votredomaine.net/sitemap.gz


Automatiser le tout <span id="Makefile"></span>
----------------------------------------


Vous pouvez soit utiliser un script appelant les commandes précédentes, ou bien un fichier Makefile, qui vous permettra de faire tout ça avec simplement la commande make.

Exemple de fichier Makefile, à adapter selon le chemin de vos fichiers (variables DESTDIR, SOURCEDIR, et le nom du dossier contenant le site (ici Rendez-vous_sur_Arrakis)) :

	SOURCEDIR=$(CURDIR)/example
	DESTDIR=$(CURDIR)/example.static

	all:
		@echo "Generate website"
		@./swx $(SOURCEDIR)
		@echo "Remove .swp"
		@find $(DESTDIR) -name '*.swp' -exec rm '{}' \;
		@echo "Generate sitemap and gzip it"
		@./swx_sitemap  $(DESTDIR) > $(DESTDIR)/sitemap.xml
		@gzip --best -c $(DESTDIR)/sitemap.xml > $(DESTDIR)/sitemap.gz
		@echo "Generate website plan"
		@./swx_plan $(DESTDIR) > $(DESTDIR)/Divers/Plan_du_site.html
		@echo "Generate atom feed"
		@./swx_atom $(SOURCEDIR) > $(DESTDIR)/atom.xml
		@echo "Generate blog news entry"
		@./swx_blog $(SOURCEDIR) 
	clean:
		rm -rf *.static
	force:
		rm $(DESTDIR)/.swx.last
		make all
	serve:
		sleep 1 && firefox http://127.0.0.1:8000 &
		cd $(DESTDIR) && python3 -m http.server 
		./swx $(SOURCEDIR)
		./swx_sitemap  $(DESTDIR) > $(DESTDIR)/sitemap.xml
		gzip --best -c $(DESTDIR)/sitemap.xml > $(DESTDIR)/sitemap.gz
		./swx_plan $(DESTDIR) > $(DESTDIR)/siteplan.html
		./swx_atom > $(DESTDIR)/feed.atom
		./swx_blog $(DESTDIR)/index.html

Voici ce que vous pourrez faire avec ce fichier présent dans votre dossier contenant swx :

 - ``make`` : fabrique/met à jour votre site
 - ``make clean`` : supprime votre site
 - ``make force`` : force la refabrication du site, même si certaines pages ne sont pas détectées comme récentes.
 - ``make serve`` : permet de tester votre site localement. Allez sur votre navigateur à la page http://localhost:8000


Le mot de la fin
---------------------------


Évitez les fichiers avec des noms bizarres, des espaces… Pour vous en
débarrasser, pensez à la commande ``detox`` (paquet debian du même nom).


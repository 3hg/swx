SOURCEDIR=$(.CURDIR)/example
DESTDIR=$(.CURDIR)/example.static

all:
	sh ./swx $(SOURCEDIR)
	./swx_sitemap  $(DESTDIR) > $(DESTDIR)/sitemap.xml
	gzip --best -c $(DESTDIR)/sitemap.xml > $(DESTDIR)/sitemap.gz
	./swx_plan $(DESTDIR) > $(DESTDIR)/siteplan.html
	./swx_atom $(SOURCEDIR) > $(DESTDIR)/feed.atom
	./swx_blog $(SOURCEDIR)

clean:
	rm -rf *.static
	rm swx.log
force:
	find $(SOURCEDIR) -exec touch {} \;
	make all
serve:
	sleep 2 && x-www-browser http://localhost:8000 &
	cd $(DESTDIR) && python3 -m http.server 
